package com.example.common

open interface UseCase<T, R> {
    suspend fun invoke(input: T): R
}