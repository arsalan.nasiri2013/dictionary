package com.example.common

interface Mapper<T,R> {
     fun map(input: T) : R
}