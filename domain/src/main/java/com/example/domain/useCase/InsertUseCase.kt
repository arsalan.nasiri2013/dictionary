package com.example.domain.useCase

import com.example.common.UseCase
import com.example.domain.model.db.DictionaryDbModel
import com.example.domain.repository.DictionaryRepository
import javax.inject.Inject

class InsertUseCase @Inject constructor(
    private val dictionaryRepository: DictionaryRepository
) : UseCase<String, DictionaryDbModel> {
    override suspend fun invoke(input: String): DictionaryDbModel {
        return dictionaryRepository.insert(
            DictionaryDbModel(
                0,
                word = input,
                translation = ""
            )
        )
    }
}