package com.example.domain.model.remote

data class Response (
    val responseData: ResponseData?
)