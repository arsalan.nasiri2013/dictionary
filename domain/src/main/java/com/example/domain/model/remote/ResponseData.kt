package com.example.domain.model.remote

data class ResponseData (
    val translatedText :String?
)