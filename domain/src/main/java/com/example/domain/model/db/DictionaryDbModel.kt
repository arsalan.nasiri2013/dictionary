package com.example.domain.model.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "dictionary")
data class DictionaryDbModel(
    @PrimaryKey(autoGenerate = true) var id : Long?,
    var word: String,
    var translation: String?
)
