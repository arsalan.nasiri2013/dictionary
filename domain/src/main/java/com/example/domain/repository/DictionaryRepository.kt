package com.example.domain.repository

import com.example.domain.model.db.DictionaryDbModel

interface DictionaryRepository {
    suspend fun insert(dictionaryDbModel: DictionaryDbModel) : DictionaryDbModel
    suspend fun delete(dictionaryDbModel: DictionaryDbModel) : DictionaryDbModel
    suspend fun getAll() : List<DictionaryDbModel>
    suspend fun translate(item:DictionaryDbModel) : DictionaryDbModel
}