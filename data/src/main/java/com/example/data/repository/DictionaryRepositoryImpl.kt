package com.example.data.repository

import com.example.data.ApiService
import com.example.data.db.DictionaryDao
import com.example.domain.model.db.DictionaryDbModel
import com.example.domain.repository.DictionaryRepository
import javax.inject.Inject

class DictionaryRepositoryImpl @Inject constructor(
    private val dictionaryDao: DictionaryDao,
    private val apiService: ApiService
) : DictionaryRepository {
    override suspend fun insert(dictionaryDbModel: DictionaryDbModel): DictionaryDbModel {
        dictionaryDbModel.id = dictionaryDao.insert(dictionaryDbModel)
        return dictionaryDbModel
    }

    override suspend fun delete(dictionaryDbModel: DictionaryDbModel): DictionaryDbModel {
        dictionaryDao.delete(dictionaryDbModel)
        return dictionaryDbModel
    }


    override suspend fun getAll(): List<DictionaryDbModel> {
        return dictionaryDao.getAll()
    }

    override suspend fun translate(item: DictionaryDbModel): DictionaryDbModel {
        val response = apiService.translate(word = item.word)
        if (response.isSuccessful) {
            item.translation = response.body()?.responseData?.translatedText
            dictionaryDao.update(item)
            return item
        }
        throw Exception()
    }
}