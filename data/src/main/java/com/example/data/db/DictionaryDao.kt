package com.example.data.db

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.example.domain.model.db.DictionaryDbModel

@Dao
interface DictionaryDao {
    @Query("SELECT * FROM dictionary ORDER BY id ASC")
    suspend fun getAll(): List<DictionaryDbModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert( dictionary: DictionaryDbModel) :Long

    @Update
    suspend fun update(dictionary: DictionaryDbModel)

    @Delete
    suspend fun delete(dictionary: DictionaryDbModel)
}