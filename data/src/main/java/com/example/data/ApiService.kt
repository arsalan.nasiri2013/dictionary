package com.example.data

import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("get")
    suspend fun translate(@Query("q") word: String,@Query("langpair") langpair:String = "en|fa"): retrofit2.Response<com.example.domain.model.remote.Response>
}