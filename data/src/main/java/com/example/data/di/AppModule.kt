package com.example.data.di

import android.content.Context
import androidx.room.Room
import com.example.data.ApiService
import com.example.data.BuildConfig
import com.example.data.db.AppDatabase
import com.example.data.db.DictionaryDao
import com.example.data.repository.DictionaryRepositoryImpl
import com.example.domain.repository.DictionaryRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {
    @Singleton
    @Provides
    fun provideDatabase(
        @ApplicationContext app: Context
    ) = Room.databaseBuilder(
        app,
        AppDatabase::class.java,
        "app_database"
    ).build()


    @Singleton
    @Provides
    fun provideDictionaryDao(db: AppDatabase) = db.dictionaryDao()


    @Singleton
    @Provides
    fun provideDictionaryRepository(
        dictionaryDao: DictionaryDao,
        apiService: ApiService
    ): DictionaryRepository {
        return DictionaryRepositoryImpl(dictionaryDao,apiService)
    }

    @Provides
    fun provideBaseUrl() = "https://api.mymemory.translated.net/"

    @Singleton
    @Provides
    fun provideOkHttpClient(@ApplicationContext app: Context) =if (BuildConfig.DEBUG){
        val loggingInterceptor =HttpLoggingInterceptor()
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)
        OkHttpClient.Builder()
            .addInterceptor(loggingInterceptor)
            .build()
    }else{
        OkHttpClient
            .Builder()
            .build()
    }



    @Singleton
    @Provides
    fun provideRetrofit(okHttpClient: OkHttpClient, BASE_URL: String): Retrofit = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .build()

    @Provides
    @Singleton
    fun provideApiService(retrofit: Retrofit) = retrofit.create(ApiService::class.java)

}

