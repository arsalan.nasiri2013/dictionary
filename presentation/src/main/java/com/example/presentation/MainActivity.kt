package com.example.presentation

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.animation.core.MutableTransitionState
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.runtime.toMutableStateList
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.lifecycle.Observer
import com.example.domain.model.db.DictionaryDbModel
import com.example.presentation.ui.theme.DictionaryTheme
import com.example.presentation.ui.theme.Purple40
import com.example.presentation.ui.theme.PurpleGrey80
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    private val viewModel: MainViewModel by viewModels()
    private var dictionaryDbModels = mutableStateListOf<DictionaryDbModel>()
    private var selectedModels: DictionaryDbModel? = null
    private var selectedState: MutableTransitionState<Boolean>? = null
    val openDialog = mutableStateOf(false)
    var isItemAdded = false
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            DictionaryTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Greeting()
                }
            }
        }
        val insertObserver = Observer<DictionaryDbModel> { word ->
            dictionaryDbModels.add(word)
        }
        viewModel.insert.observe(this, insertObserver)
        val translateObserver = Observer<DictionaryDbModel> { word ->
            dictionaryDbModels.find {
                it.id == selectedModels?.id
            }?.translation = word.translation
            selectedState?.targetState = true
        }
        viewModel.translate.observe(this, translateObserver)

        val deleteObserver = Observer<DictionaryDbModel> { word ->
            dictionaryDbModels.remove(word)
        }
        viewModel.delete.observe(this, deleteObserver)
        val loadingObserver = Observer<Boolean> { isLoading ->
           openDialog.value = isLoading
        }
        viewModel.isLoading.observe(this, loadingObserver)

        val items = Observer<List<DictionaryDbModel>> { words ->
            dictionaryDbModels.addAll(words.toMutableStateList())
        }
        viewModel.list.observe(this, items)

    }

    @SuppressLint("UnusedMaterial3ScaffoldPaddingParameter")
    @OptIn(ExperimentalMaterial3Api::class)
    @Composable
    fun Greeting() {
        if (openDialog.value) {
            AlertDialog(
                containerColor = Color.Transparent,
                title = { Row (horizontalArrangement = Arrangement.Center, modifier = Modifier.fillMaxWidth()){
                    CircularProgressIndicator(color = Color.White)
                } },
                onDismissRequest = {  },

                confirmButton = {

                })
        }
        Column {
            val word = remember { mutableStateOf(TextFieldValue()) }
            TopAppBar(
                colors = TopAppBarDefaults.smallTopAppBarColors(containerColor = Purple40),
                title = {
                    Text(text = "Dictionary", color = Color.White)
                },
            )
            Spacer(modifier = Modifier.height(8.dp))
            Row(
                Modifier
                    .padding(horizontal = 16.dp)
                    .height(48.dp)
            ) {
                TextField(
                    value = word.value, onValueChange = {
                        word.value = it
                    },
                    Modifier
                        .height(48.dp)
                        .weight(1f),
                    shape = RoundedCornerShape(8.dp),
                    trailingIcon = {
                        Icon(
                            Icons.Filled.Add, "",
                            tint = Purple40,
                            modifier = Modifier.clickable {
                                if (word.value.text.isNotEmpty())
                                    viewModel.insert(word.value.text)
                            })
                    },
                    colors = TextFieldDefaults.textFieldColors(
                        containerColor = PurpleGrey80,
                        focusedIndicatorColor = Color.Transparent, //hide the indicator
                        unfocusedIndicatorColor = PurpleGrey80
                    )
                )

            }
            Spacer(modifier = Modifier.height(8.dp))
            generateList()
        }


    }

    @SuppressLint("UnrememberedMutableState")
    @Composable
    fun generateList() {
        var currentSize by rememberSaveable { mutableStateOf(dictionaryDbModels.size) }
        val isItemAdded by mutableStateOf(dictionaryDbModels.size > currentSize)
        val listState = rememberLazyListState()
        LaunchedEffect(isItemAdded) { //Won't be called upon item deletion
            if (isItemAdded) {
                listState.animateScrollToItem(dictionaryDbModels.size)
                currentSize = dictionaryDbModels.size
            }
        }
        LazyColumn(
            state = listState,
            contentPadding = PaddingValues(8.dp),
            reverseLayout = true
        ) {
            items(dictionaryDbModels) { item ->
                val state = remember {
                    MutableTransitionState(false).apply {
                        // Start the animation immediately.
                        targetState = false
                    }
                }

                Row(modifier = Modifier
                    .clickable {
                        if (item.translation.isNullOrEmpty()) {
                            selectedModels = item
                            selectedState = state
                            viewModel.translate(item)
                        } else {
                            state.targetState = !state.targetState
                        }
                    }
                    .fillMaxSize()) {
                    Card(
                        shape = RoundedCornerShape(5.dp),
                        colors = CardDefaults.cardColors(containerColor = Color.White),
                        elevation = CardDefaults.cardElevation(
                            defaultElevation = 10.dp
                        ),
                        modifier = Modifier
                            .padding(8.dp)
                            .fillMaxWidth()
                    ) {
                        Column(modifier = Modifier.padding(horizontal = 8.dp, vertical = 16.dp)) {

                            Row {
                                Text(
                                    text = item.word,
                                    color = Color.Black,
                                    modifier = Modifier.weight(1f)
                                )
                                Icon(
                                    Icons.Filled.Delete, "",
                                    tint = Purple40,
                                    modifier = Modifier.clickable {
                                         viewModel.delete(item)
                                    })
                            }
                            if (state.targetState) {
                                Spacer(modifier = Modifier.height(10.dp))
                                item.translation?.let {
                                    Text(
                                        text = it,
                                        textAlign = TextAlign.Right,
                                        modifier = Modifier.fillMaxSize()
                                    )
                                }
                            }

                        }
                    }

                }
                // Spacer(modifier = Modifier.height(8.dp))
            }
        }
    }


}


