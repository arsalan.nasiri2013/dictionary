package com.example.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.domain.model.db.DictionaryDbModel
import com.example.domain.repository.DictionaryRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val dictionaryRepository: DictionaryRepository
) : ViewModel() {
    init {
        getAllWord()
    }

    val isLoading = MutableLiveData<Boolean>()
    val insert = MutableLiveData<DictionaryDbModel>()
    val translate = MutableLiveData<DictionaryDbModel>()
    val delete = MutableLiveData<DictionaryDbModel>()
    val list = MutableLiveData<List<DictionaryDbModel>>()

    fun insert(word: String) {
        viewModelScope.launch {
            val result =
                dictionaryRepository.insert(DictionaryDbModel(null, word = word, translation = ""))
            insert.value = result
        }
    }

    fun translate(word: DictionaryDbModel) {
        isLoading.value = true
        viewModelScope.launch {
            val result =
                dictionaryRepository.translate(word)
            translate.value = result
            isLoading.value = false
        }
    }

    fun delete(word: DictionaryDbModel) {
        viewModelScope.launch {
            val result = dictionaryRepository.delete(word)
            delete.value = result
        }
    }

    fun getAllWord() {
        viewModelScope.launch {
            val result = dictionaryRepository.getAll()
            list.value = result
        }
    }
}